#Temblar 
Temblar is a single-page application built with Blazor WebAssembly .

## Project contributors
* André Skjelin Ottosen
* Irfan Nazand
* Nicolas Anderson
* Regine Giskegjerde Urtegård


## Installation
Navigate to folder
```bash
cd ../selectedfolder/
```
### Clone Repository HTTPS

```bash
git clone https://tradesolution.visualstudio.com/DefaultCollection/Temblar/_git/Temblar
```
Generate Git Cedentials

## Open project in Visual Studio
Open Visual Studio -> open a project or solution -> Temblar.sln
Build solution press F8

## Run in  Visual Studio
Pun without debugging for hot reloading 
Press Ctrl + F5 

## Authentication
Temblar uses sign in with Microsoft authentication where users must be registered in Tradesolution's own Active directory to get access.


## API documentation 
###Temblar.Server  API Documentation v1 

The Swagger UI presentation of the API documentation can be available by running Temblar locally and visiting this URL https://localhost:<port>/swagger/index.html 

### Account 

GET /MicrosoftIdentity /Account /SignIn /{scheme} 

GET /MicrosoftIdentity /Account /Challenge /{scheme} 

GET /MicrosoftIdentity /Account /SignOut /{scheme} 

GET /MicrosoftIdentity /Account /ResetPassword /{scheme} 

GET /MicrosoftIdentity /Account /EditProfile /{scheme} 

 
### Authorization 

DELETE /api /Authorization /clear 

PUT /api /Authorization /client 

GET /api /Authorization 

 
### ClientDevice 

DELETE /api /ClientDevice /clear 

GET /api /ClientDevice 

POST /api /ClientDevice 

GET /api /ClientDevice /{id} 

PUT /api /ClientDevice /{id} 

DELETE /api /ClientDevice /{id} 


### CustomNotifications 

DELETE /api /CustomNotifications /clear 

GET /api /CustomNotifications 

POST /api /CustomNotifications 

GET /api /CustomNotifications /{id} 

PUT /api /CustomNotifications /{id} 

DELETE /api /CustomNotifications /{id} 

 
### Events 

DELETE /api /Events /clear 

GET /api /Events 

POST /api /Events 

GET /api /Events /{id} 

PUT /api /Events /{id} 

DELETE /api /Events /{id} 

 
### EventTypes 

DELETE /api /EventTypes /clear 

GET /api /EventTypes 

POST /api /EventTypes 

GET /api /EventTypes /{id} 

PUT /api /EventTypes /{id} 

DELETE /api /EventTypes /{id} 

 

### Notifications 

PUT /api /Notifications /MarkAllMyNotificationsAsRead 

PUT /api /Notifications /MarkAsRead 

PUT /api /Notifications /MarkAllMyNotificationsAsSeen 

GET /api /Notifications /MyUnseenNotificationCount 

GET /api /Notifications /MyLatestNotifications 

GET /api /Notifications /MyNotificationsArchive 

 
### Sources 

DELETE /api /Sources 

POST /api /Sources 

GET /api /Sources /admin 

GET /api /Sources /{id} 

PUT /api /Sources /{id} 

DELETE /api /Sources /admin /{id} 

 
### SourceSubscriptions 

DELETE /api /SourceSubscriptions /clear 

GET /api /SourceSubscriptions 

POST /api /SourceSubscriptions 

GET /api /SourceSubscriptions /{id} 

PUT /api /SourceSubscriptions /{sourceId}&{temblarUserid} 

DELETE /api /SourceSubscriptions /{sourceId}&{temblarUserId} 

 
### Subscriptions 

DELETE /api /Subscriptions /clear 

GET /api /Subscriptions 

POST /api /Subscriptions 

GET /api /Subscriptions /{id} 

PUT /api /Subscriptions /{eventTypeId}&{temblarUserId} 

DELETE /api /Subscriptions /{eventTypeId}&{temblarUserId} 


### TemblarUsers 

DELETE /api /TemblarUsers /clear 

GET /api /TemblarUsers 

POST /api /TemblarUsers 

GET /api /TemblarUsers /{id} 

PUT /api /TemblarUsers /{id} 

DELETE /api /TemblarUsers /{id} 


### TemblarUserSubscriptions 

GET /api /personalsubscribers /{temblarUserId} 

GET /api /sources 

PUT /api /{subscriberId} /subscribetosource 

PUT /api /{subscriberId} /unsubscribetosource 

 
### Schemas 

SourceSubscription  

Source  

CustomNotification  

Event  

EventType  

Subscription  

TemblarUser  

ClientDevice  

MarkNotificationsAsReadCommand  

MarkAllNotificationsAsSeenCommand  

Notification  

PushNotificationDto  

PersonalSubscriberDTO  

SubscribeToSourceCommand  

UnsubscribeToSourceCommand 

 

## Installation of PWA
#### Android
1. Open Temblar in Google Chrome
2. Tap Add to home screen.
3. Follow the onscreen instructions to install.

#### iOS
1. Open Temblar in Safari.
2. Press the "Share" button.
3. Select "Add to Home Screen."

###### Note: PWA Push notifications are not supported on iOS.

#### Desktop
1. Open Temblar in Google Chrome. 
2. Tap the (+) icon on the right side of the address bar to download the application.

###### Note: The (+) icon is not visible in Incognito mode


