﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Temblar.Shared.Models;

namespace Temblar.Client.Services
{
    public class AuthorizationService
    {
        private readonly HttpClient httpClient;

        private readonly TemblarAppState appState;

        private readonly HttpErrorService httpErrorService;



        public AuthorizationService(HttpClient httpClient, TemblarAppState appState, HttpErrorService httpErrorService)
        {
            this.httpClient = httpClient;
            this.appState = appState;
            this.httpErrorService = httpErrorService;
        }

        public async Task<TemblarUser> AuthorizeMe()
        {
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetAsync("/api/Authorization");
                if (result.IsSuccessStatusCode)
                {
                    appState.temblarUser = await result.Content.ReadFromJsonAsync<TemblarUser>();
                }
                else if(!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            return null;
        }

        public async Task AddClient(ClientDevice clientDevice)
        {
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsJsonAsync("/api/Authorization/client", clientDevice);

                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
        }

    }
}
