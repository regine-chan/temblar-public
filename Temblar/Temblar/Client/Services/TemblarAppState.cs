﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.DTOs;
using Temblar.Shared.Models;

namespace Temblar.Client.Services
{
    public class TemblarAppState
    {
        private readonly LocalStorageService localStorageService;

        private readonly NavigationManager navigationManager;

        private readonly SignOutSessionStateManager signOutSessionStateManager;

        public TemblarUser temblarUser { get; set; }

        public List<string> sources { get; set; }

        public HashSet<KeyValuePair<string, Boolean>> subscriptions { get; set; }

        public PersonalSubscriberDTO personalSubscriberDTO { get; set; }

        public List<PushNotificationDto> notReadNotifications { get; set; }

        public List<PushNotificationDto> readNotifications { get; set; }

        public Boolean isLoading { get; set; }

        public string errorMessage { get; set; }

        public TemblarAppState(LocalStorageService localStorageService, NavigationManager navigation, SignOutSessionStateManager sessionStateManager)
        {
            this.localStorageService = localStorageService;
            this.navigationManager = navigation;
            this.signOutSessionStateManager = sessionStateManager;
            this.temblarUser = null;
            this.sources = new List<string>();
            this.subscriptions = new HashSet<KeyValuePair<string, bool>>();
            this.notReadNotifications = new List<PushNotificationDto>();
            this.readNotifications = new List<PushNotificationDto>();
            this.personalSubscriberDTO = null;
            this.isLoading = false;
        }

        public async Task SignOut()
        {
            await localStorageService.ClearAsync();
            await signOutSessionStateManager.SetSignOutState();
            navigationManager.NavigateTo("authentication/logout");
            navigationManager.NavigateTo("/");
        }

        public async Task loadTemblarAppState()
        {
            await getLocalTemblarUser();
            await getLocalNotReadNotifications();
            await getLocalSubscriptions();
            await getLocalPersonalSubscriberDTO();
            await getLocalSources();
            await getLocalReadNotifications();
        }

        public async Task setTemblarAppState()
        {
            await setLocalNotReadNotifications();
            await setLocalPersonalSubscriberDTO();
            await setLocalSubscriptions();
            await setLocalTemblarUser();
            await setLocalSources();
            await setLocalReadNotifications();
        }

        private async Task setLocalTemblarUser()
        {
                try
                {
                    await localStorageService.SetItemAsync("temblarUser", temblarUser);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        private async Task setLocalSources()
        {
                try
                {
                    await localStorageService.SetItemAsync("sources", sources);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

        }

        private async Task setLocalSubscriptions()
        {
                try
                {
                    await localStorageService.SetItemAsync("subscriptions", subscriptions);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

        }

        private async Task setLocalPersonalSubscriberDTO()
        {
                try
                {
                    await localStorageService.SetItemAsync("personalSubscriberDTO", personalSubscriberDTO);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        private async Task setLocalNotReadNotifications()
        {
                try
                {
                    await localStorageService.SetItemAsync("notReadNotifications", notReadNotifications.OrderByDescending(nrn => nrn.Notification.TriggeredAt).Take(10));

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        private async Task setLocalReadNotifications()
        {
                try
                {
                    await localStorageService.SetItemAsync("ReadNotifications", readNotifications.OrderByDescending(rn => rn.Notification.TriggeredAt).Take(10));

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        private async Task getLocalReadNotifications()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<List<PushNotificationDto>>("ReadNotifications");
                if (result != null)
                {
                    readNotifications = result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task getLocalNotReadNotifications()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<List<PushNotificationDto>>("notReadNotifications");
                if (result != null)
                {
                    notReadNotifications = result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task getLocalTemblarUser()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<TemblarUser>("temblarUser");
                if (result != null)
                {
                    temblarUser = result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task getLocalSources()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<List<string>>("sources");
                if (result != null)
                {
                    sources = result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task getLocalSubscriptions()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<HashSet<KeyValuePair<string, Boolean>>>("subscriptions");
                if (result != null)
                {
                    subscriptions = result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task getLocalPersonalSubscriberDTO()
        {
            try
            {
                var result = await localStorageService.GetItemAsync<PersonalSubscriberDTO>("personalSubscriberDTO");
                if (result != null)
                {
                    personalSubscriberDTO = result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
