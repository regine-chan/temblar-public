﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Temblar.Shared.DTOs;
using Temblar.Shared.Commands;
using Blazored.LocalStorage;
using System.Net;

namespace Temblar.Client.Services
{
    public class SourceService
    {
        private readonly HttpClient httpClient;

        private readonly TemblarAppState appState;

        private readonly HttpErrorService httpErrorService;

        public List<string> Sources { get; set; }

        public PersonalSubscriberDTO temblarUser { get; set; }

        public SourceService(HttpClient httpClient, TemblarAppState appState, HttpErrorService httpErrorService)
        {
            this.httpClient = httpClient;
            this.appState = appState;
            this.httpErrorService = httpErrorService;
            temblarUser = null;
            Sources = new List<string>();
        }

        public async Task GetAllSubscribedResources(Guid temblarUserId)
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetAsync("/api/personalsubscribers/" + temblarUserId);
                if (result.IsSuccessStatusCode)
                {
                    appState.personalSubscriberDTO = await result.Content.ReadFromJsonAsync<PersonalSubscriberDTO>();
                } 
                else if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task GetAllSources()
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetAsync("/api/sources");
                if (result.IsSuccessStatusCode)
                {
                    appState.sources = await result.Content.ReadFromJsonAsync<List<string>>();
                }
                else if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }

            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task SubscribeSourceSubscription(Guid subscriberId, string source)
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsJsonAsync("/api/" + subscriberId + "/subscribetosource", new SubscribeToSourceCommand { Source = source });

                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }

            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task UnsubscribeSourceSubscription(Guid subscriberId, string source)
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsJsonAsync("/api/" + subscriberId + "/unsubscribetosource", new UnsubscribeToSourceCommand { Source = source });
                
                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }

        }


    }
}
