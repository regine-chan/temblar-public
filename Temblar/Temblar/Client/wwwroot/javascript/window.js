﻿// Checks if app is in standalone mode
function isStandAlone() {
    const displayMode = '(display-mode: standalone)';
    return (window.navigator.standalone || window.matchMedia(displayMode).matches); 
}

// Checks if device is on iOS 
function isiOS() {
    const userAgent = window.navigator.userAgent.toLowerCase();
    const iOSDevices = /iphone|ipad|ipod/;
    return iOSDevices.test(userAgent);
}

// Checks if device is on Android
function isAndroid() {
    const userAgent = window.navigator.userAgent.toLowerCase();
    const androidDevices = /android/;
    return androidDevices.test(userAgent);
}



