﻿
// Retrieves id token from session storage and stores it in local storage
function setTokenFromSessionToLocal() {

    // Retrieves id token from session storage
    const idTokenSessionstorage = sessionStorage.getItem("msal.idtoken");

    // Checks if idTokenSessionstorage is not empty
    if (idTokenSessionstorage != null) {

        // Sets id token in local storage
        localStorage.setItem("msal.idtoken", idTokenSessionstorage);
    }
}

// Retrieves id token from local storage and stores it in session storage
function setTokenFromLocalToSession() {

    // Retrieves id token from local storage
    const idTokenLocalStorage = localStorage.getItem("msal.idtoken");

    // Retrieves id token from session storage
    const idTokenSessionstorage = sessionStorage.getItem("msal.idtoken");

    // Checks if idTokenLocalStorage is not empty and if idTokenSessionstorage is empty
    if (idTokenLocalStorage != null && idTokenSessionstorage == null) {

        // Sets id token in session storage
        sessionStorage.setItem("msal.idtoken", idTokenLocalStorage);
        return true;
    }
}


