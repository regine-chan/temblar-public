// Prevent caching on testing
self.addEventListener('fetch', () => { });


self.addEventListener('push', event => {
    const payload = event.data.json();

    if ("Notification" in payload) {
        event.waitUntil(
            self.registration.showNotification(payload.Notification.DisplayName, {
                body: payload.Notification.DisplayMessage,
                icon: 'favicon-96x96.png',
                vibrate: [100, 50, 100],
                data: { url: payload.Notification.UserInterfaceUrl }
            })
        );
    } else {
        const unseen = payload.Unseen;
        navigator.setAppBadge(unseen).catch((error) => {
            console.log(error);
        });
        event.waitUntil(
            self.registration.showNotification("Temblar", {
                body: $`Du har {unseen} usette notifikasjoner.`,
                icon: 'favicon-96x96.png',
                vibrate: [100, 50, 100],
                data: { url: payload.Notification.UserInterfaceUrl }
            })
        );
    }
    

});

self.addEventListener('notificationclick', event => {
    self.registration.getNotifications().then(notifications => {
        notifications.forEach(notification => {
            notification.close();
        });
    });
    navigator.setAppBadge(0);
    event.waitUntil(clients.openWindow("CLIENT_URL"));
});

self.addEventListener('sync', event => {
    event.waitUntil(async () => {
        try {
            const unseen = await fetch("REST_URL").then(res => {
                if (res.ok) {
                    return res.json();
                    console.log("sync event");
                } else {
                    return 0;
                }
            });
            if (unseen > 0) {
                navigator.setAppBadge(unseen).catch(error => {
                    console.log(error);
                });
            }
        }
        catch (e) {
            console.log(e);
        }
    });

});