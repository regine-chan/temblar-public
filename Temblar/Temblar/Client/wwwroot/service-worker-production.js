﻿// In development, always fetch from the network and do not enable offline support.
// This is because caching would make development more difficult (changes would not
// be reflected on the first load after each change).
self.importScripts('./service-worker-assets.js');
self.addEventListener('install', event => event.waitUntil(onInstall(event)));
self.addEventListener('activate', event => event.waitUntil(onActivate(event)));
self.addEventListener('fetch', event => event.respondWith(onFetch(event)));

const cacheNamePrefix = 'offline-cache-';
const cacheName = `${cacheNamePrefix}${self.assetsManifest.version}`;
const offlineAssetsInclude = [/\.dll$/, /\.pdb$/, /\.wasm/, /\.html/, /\.js$/, /\.json$/, /\.css$/, /\.woff$/, /\.png$/, /\.jpe?g$/, /\.gif$/, /\.ico$/];
const offlineAssetsExclude = [/^service-worker\.js$/];

async function onInstall(event) {
    console.info('Service worker: Install');

    // Fetch and cache all matching items from the assets manifest
    const assetsRequests = self.assetsManifest.assets
        .filter(asset => offlineAssetsInclude.some(pattern => pattern.test(asset.url)))
        .filter(asset => !offlineAssetsExclude.some(pattern => pattern.test(asset.url)))
        .map(asset => new Request(asset.url, { integrity: asset.hash }));
    await caches.open(cacheName).then(cache => cache.addAll(assetsRequests));
}

async function onActivate(event) {
    console.info('Service worker: Activate');

    // Delete unused caches
    const cacheKeys = await caches.keys();
    await Promise.all(cacheKeys
        .filter(key => key.startsWith(cacheNamePrefix) && key !== cacheName)
        .map(key => caches.delete(key)));
}

async function onFetch(event) {
    let cachedResponse = null;
    if (event.request.method === 'GET') {
        // For all navigation requests, try to serve index.html from cache
        // If you need some URLs to be server-rendered, edit the following check to exclude those URLs
        const shouldServeIndexHtml = event.request.mode === 'navigate';

        const request = shouldServeIndexHtml ? 'index.html' : event.request;
        const cache = await caches.open(cacheName);
        cachedResponse = await cache.match(request);
    }

    return cachedResponse || fetch(event.request);
}


self.addEventListener('push', event => {
    const payload = event.data.json();

    if ("Notification" in payload) {
        event.waitUntil(
            self.registration.showNotification(payload.Notification.DisplayName, {
                body: payload.Notification.DisplayMessage,
                icon: 'favicon-96x96.png',
                vibrate: [100, 50, 100],
                data: { url: payload.Notification.UserInterfaceUrl }
            })
        );
    } else {
        const unseen = payload.Unseen;
        navigator.setAppBadge(unseen).catch((error) => {
            console.log(error);
        });
        event.waitUntil(
            self.registration.showNotification("Temblar", {
                body: $`Du har {unseen} usette notifikasjoner.`,
                icon: 'favicon-96x96.png',
                vibrate: [100, 50, 100],
                data: { url: payload.Notification.UserInterfaceUrl }
            })
        );
    }


});

self.addEventListener('notificationclick', event => {
    self.registration.getNotifications().then(notifications => {
        notifications.forEach(notification => {
            notification.close();
        });
    });
    navigator.setAppBadge(0);
    event.waitUntil(clients.openWindow("CLIENT_URL"));
});

self.addEventListener('sync', event => {
    event.waitUntil(async () => {
        try {
            const unseen = await fetch("REST_URL").then(res => {
                if (res.ok) {
                    return res.json();
                    console.log("sync event");
                } else {
                    return 0;
                }
            });
            if (unseen > 0) {
                navigator.setAppBadge(unseen).catch(error => {
                    console.log(error);
                });
            }
        }
        catch (e) {
            console.log(e);
        }
    });

});