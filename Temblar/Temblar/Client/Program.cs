using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Temblar.Client.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Blazored.LocalStorage;
using Blazored.Toast;
using Blazored.Toast.Services;

namespace Temblar.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.Services.AddSingleton< NotificationService>();
            builder.Services.AddSingleton<AuthorizationService>();
            builder.Services.AddSingleton<SourceService>();
            builder.Services.AddSingleton<TemblarAppState>();
            builder.Services.AddSingleton<LocalStorageService>();
            builder.Services.AddSingleton<StorageService>();
            builder.Services.AddSingleton<SignOutSessionStateManager>();
            builder.Services.AddSingleton<WindowService>();
            builder.Services.AddSingleton<HttpErrorService>();
            builder.Services.AddBlazoredToast();

            builder.RootComponents.Add<App>("app");
          
            builder.Services.AddHttpClient("CLIENT_NAME", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
                .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();
     
            builder.Services.AddSingleton(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("CLIENT_NAME"));


            
            builder.Services.AddMsalAuthentication(options =>
            {
                    var authentication = options.ProviderOptions.Authentication;
                    authentication.Authority = "AAD_AUTHORITY";
                    authentication.ClientId = "AAD_CLIENT_ID";
                    authentication.ValidateAuthority = true;

                    var DefaultAccessTokenScopes = options.ProviderOptions.DefaultAccessTokenScopes;
                    DefaultAccessTokenScopes.Add("AAD_ACCESS_TOKEN_SCOPES");

            });

            await builder.Build().RunAsync();
        }
    }
}
