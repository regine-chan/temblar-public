﻿using AutoMapper.Internal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Temblar.Shared.Commands;
using Temblar.Shared.DTOs;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [Authorize]
    [Route("/api/[controller]")]
    [ApiController]
    public class Notifications : ControllerBase
    {
        private TemblarDbContext ctx;

        public Notifications(TemblarDbContext ctx)
        {
            this.ctx = ctx;
        }

        // PUT /api/Notifications/MarkAllMyNotificationsAsRead
        [HttpPut("MarkAllMyNotificationsAsRead")]
        public async Task<ActionResult> PutMarkAllMyNotificationsAsRead()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                TemblarUser user = null;
                try
                {
                    user = await ctx.TemblarUser.Where(tu => tu.Username == email).Include(tu => tu.CustomNotifications).FirstOrDefaultAsync();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    user.CustomNotifications.Where(cn => cn.ReadAt == null).ToList().ForEach(item =>
                    {
                        item.ReadAt = new DateTimeOffset(DateTime.Now);
                    });

                    int changes = await ctx.SaveChangesAsync();

                    if (changes > 0)
                    {
                        return NoContent();
                    }

                    return NotFound();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT /api/Notifications/MarkAsRead
        [HttpPut("MarkAsRead")]
        public async Task<ActionResult> PutMarkAsRead([FromBody] MarkNotificationsAsReadCommand markNotificationsAsReadCommand)
        {

            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                TemblarUser user = null;
                try
                {
                    user = await ctx.TemblarUser.Where(tu => tu.Username == email).Include(tu => tu.CustomNotifications).FirstOrDefaultAsync();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    user.CustomNotifications.ToList().ForEach(item =>
                {
                    if (markNotificationsAsReadCommand.ReadIds.Contains(item.Id))
                    {
                        item.ReadAt = new DateTimeOffset(DateTime.Now);

                    }
                });

                    int changes = await ctx.SaveChangesAsync();

                    if (changes > 0)
                    {
                        return NoContent();
                    }

                    return Ok();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }


        }

        // PUT /api/Notifications/MarkAllMyNotificationsAsSeen
        [HttpPut("MarkAllMyNotificationsAsSeen")]
        public async Task<ActionResult> PutMarkAllNotificationsAsSeenCommand([FromBody] MarkAllNotificationsAsSeenCommand? markAllNotificationsAsSeenCommand)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                TemblarUser user = null;
                try
                {
                    user = await ctx.TemblarUser.Where(tu => tu.Username == email).Include(tu => tu.CustomNotifications).FirstOrDefaultAsync();
                    if (user == null)
                    {
                        return NotFound();
                    }

                    Event laterThanEvent = null;
                    List<CustomNotification> toMarkSeen = new List<CustomNotification>();

                    if (markAllNotificationsAsSeenCommand != null)
                    {
                        laterThanEvent = ctx.CustomNotification.Where(cn => cn.Id == markAllNotificationsAsSeenCommand.FromNotificationAndOlderId).Select(cn => cn.Event).FirstOrDefault();

                        toMarkSeen = ctx.CustomNotification.Where(cu => cu.Event.TriggeredAt >= laterThanEvent.TriggeredAt).Where(cu => cu.TemblarUser.Id == user.Id).ToList();
                    }
                    else
                    {
                        toMarkSeen = user.CustomNotifications.Where(cn => cn.SeenAt == null).ToList();
                    }

                    toMarkSeen.ForEach(item =>
                        {
                            item.SeenAt = new DateTimeOffset(DateTime.Now);
                        });

                    int changes = await ctx.SaveChangesAsync();

                    if (changes > 0)
                    {
                        return NoContent();
                    }
                    return Ok();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET /api/Notifications/MyUnseenCustomNotificationCount
        [HttpGet("MyUnseenNotificationCount")]
        public async Task<ActionResult<int>> GetMyUnseenCustomNotificationCount()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                try
                {
                    int result = await ctx.TemblarUser.Where(u => u.Username == email).Select(u => u.CustomNotifications.Where(n => n.SeenAt == null)).CountAsync();
                    return Ok(result);
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET /api/Notifications/MyLatestNotifications
        [HttpGet("MyLatestNotifications")]
        public async Task<ActionResult<IEnumerable<PushNotificationDto>>> GetMyLatestCustomNotifications()
        {
            const int size = 10;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                List<CustomNotification> queryResult = new List<CustomNotification>();
                try
                {
                    queryResult = await ctx.TemblarUser.Where(u => u.Username == email).SelectMany(u => u.CustomNotifications.Where(cn => cn.ReadAt == null)).Include(cn => cn.Event).Include(cn => cn.Event.Source).OrderByDescending(cn => cn.Event.TriggeredAt).Take(size).ToListAsync();

                    if (queryResult.Count == 0)
                    {
                        return NoContent();
                    }

                    List<PushNotificationDto> result = new List<PushNotificationDto>();
                    queryResult.ForEach(item =>
                    {
                        result.Add(new PushNotificationDto
                        {
                            Email = email,
                            Notification = CustomMapper(item)
                        });
                    });

                    return Ok(result);
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET /api/Notifications/MyNotificationsArchive
        [HttpGet("MyNotificationsArchive")]
        public async Task<ActionResult<IEnumerable<PushNotificationDto>>> GetMyNotificationsArchive()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                List<CustomNotification> queryResult = new List<CustomNotification>();
                try
                {
                    queryResult = await ctx.TemblarUser.Where(u => u.Username == email).SelectMany(u => u.CustomNotifications.Where(cn => cn.ReadAt != null)).Include(cn => cn.Event).Include(cn => cn.Event.Source).OrderByDescending(cn => cn.Event.TriggeredAt).ToListAsync();

                    if (queryResult.Count == 0)
                    {
                        return NoContent();
                    }

                    List<PushNotificationDto> result = new List<PushNotificationDto>();
                    queryResult.ForEach(item =>
                    {
                        result.Add(new PushNotificationDto
                        {
                            Email = email,
                            Notification = CustomMapper(item)
                        });
                    });

                    return Ok(result);
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        private Notification CustomMapper(CustomNotification not)
        {
            return new Notification
            {
                Id = not.Id,
                DisplayName = not.Event.DisplayName,
                Source = not.Event.Source.SourceName,
                Payload = not.Event.Payload,
                TriggeredBy = not.Event.TriggeredBy,
                TriggeredAt = not.Event.TriggeredAt,
                SeenAt = not.SeenAt,
                ReadAt = not.ReadAt,
                DisplayMessage = not.Event.DisplayMessage,
                UserInterfaceUrl = not.Event.UserInterfaceUrl
            };
        }
    }
}