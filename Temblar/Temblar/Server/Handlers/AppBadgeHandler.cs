﻿using Coravel.Invocable;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Models;
using WebPush;
using Temblar.Shared.DTOs;
using System.Text.Json;

namespace Temblar.Server.Handlers
{
    public class AppBadgeHandler : IInvocable
    {

        private readonly TemblarDbContext ctx;
        public AppBadgeHandler(TemblarDbContext ctx)
        {
            this.ctx = ctx;
        }

        public async Task Invoke()
        {
            var publicKey = "PUBLIC_KEY";
            var privateKey = "PRIVATE_KEY";
            VapidDetails vapidDetails = new VapidDetails("mail", publicKey, privateKey);
            WebPushClient webPushClient = new WebPushClient();

            List<TemblarUser> users = await ctx.TemblarUser.Include(tu => tu.CustomNotifications).Include(tu => tu.ClientDevices).ToListAsync();

            users.ForEach(user =>
            {    
                int unseen = user.CustomNotifications.Where(cu => cu.SeenAt == null).Count();
                if(unseen > 0)
                {
                    UnseenNotifications unseenNotifications = new UnseenNotifications { Unseen = unseen };
                    user.ClientDevices.ToList().ForEach(async cd =>
                    {
                        PushSubscription ps = new PushSubscription(cd.Url, cd.P256dh, cd.Auth);
                        try
                        {
                            var payload = JsonSerializer.Serialize(unseenNotifications);
                            await webPushClient.SendNotificationAsync(ps, payload, vapidDetails);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }                       
                    });
                }
            });
        }
    }
}
