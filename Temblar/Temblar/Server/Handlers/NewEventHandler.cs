﻿using AutoMapper.Internal;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Temblar.Shared.DTOs;
using Temblar.Shared.Models;
using WebPush;

namespace Temblar.Server.Handlers
{
    public class NewEventHandler
    {
        private readonly TemblarDbContext ctx;

        public NewEventHandler(TemblarDbContext context)
        {
            this.ctx = context;
        }

        public async Task<Boolean> OnNewEvent(Event ev)
        {
            List<TemblarUser> temblarUsers = null;
            if (ev.SourceId != null)
            {
                try
                {
                    temblarUsers = await ctx.TemblarUser.Where(tu => tu.SourceSubscriptions.Any(s => s.SourceId == ev.SourceId)).Include(ts => ts.ClientDevices).ToListAsync();
                }
                catch (Exception e)
                {
                    return false;
                }

            }
            else if (ev.EventTypeId != null)
            {
                try
                {
                    temblarUsers = await ctx.TemblarUser.Where(tu => tu.Subscriptions.Any(s => s.EventTypeId == ev.EventTypeId)).Include(ts => ts.ClientDevices).ToListAsync();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            else if (temblarUsers == null || temblarUsers.Count == 0)
            {
                return false;
            }

            temblarUsers.ForEach(user =>
            {
                if (user.CustomNotifications == null)
                {
                    user.CustomNotifications = new List<CustomNotification>();
                }
                user.CustomNotifications.Add(new CustomNotification { TemlbarUserId = user.Id, EventId = ev.Id });
            });

            try
            {
                var Changes = await ctx.SaveChangesAsync();
                if (Changes > 0)
                {
                    await SendNotifications(temblarUsers, ev.Id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task SendNotifications(List<TemblarUser> temblarUsers, Guid eventId)
        {
            var publicKey = "PUBLIC_KEY";
            var privateKey = "PRIVATE_KEY";

            VapidDetails vapidDetails = new VapidDetails("mail", publicKey, privateKey);
            WebPushClient webPushClient = new WebPushClient();

            await Task.Run(() => 
            {
                temblarUsers.ForEach(item => {
                    
                        PushNotificationDto pdto = CustomMapper(GetCustomNotification(item.Id, eventId));
                        var payload = JsonSerializer.Serialize(pdto);

                        item.ClientDevices.ForAll(async cd =>
                        {
                            PushSubscription ps = new PushSubscription(cd.Url, cd.P256dh, cd.Auth);
                            try
                            {
                                await webPushClient.SendNotificationAsync(ps, payload, vapidDetails);
                                Console.WriteLine("\n Success \n");

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        });
                    
                });
            });
            
        }

        private CustomNotification GetCustomNotification(Guid temblarUserId, Guid eventId)
        {
            return  ctx.CustomNotification
                    .Where(cm => cm.EventId == eventId)
                    .Where(cm => cm.TemlbarUserId == temblarUserId)
                    .Include(cm => cm.TemblarUser)
                    .Include(cm => cm.Event)
                    .FirstOrDefault();
        }

        private PushNotificationDto CustomMapper(CustomNotification cn)
        {
            return new PushNotificationDto
            {
                Email = cn.TemblarUser.Username,
                Notification = new Notification
                {
                    Id = cn.Id,
                    DisplayName = cn.Event.DisplayName,
                    Source = cn.Event.Source.SourceName,
                    Payload = cn.Event.Payload,
                    TriggeredBy = cn.Event.TriggeredBy,
                    TriggeredAt = cn.Event.TriggeredAt,
                    SeenAt = cn.SeenAt,
                    ReadAt = cn.ReadAt,
                    DisplayMessage = cn.Event.DisplayMessage,
                    UserInterfaceUrl = cn.Event.UserInterfaceUrl
                }
            };
        }


    }

}