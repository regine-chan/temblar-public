﻿using Microsoft.EntityFrameworkCore;
using System;
using Temblar.Shared.Models;

namespace Temblar.Server
{
    public class TemblarDbContext : DbContext
    {
        public DbSet<Event> Event { get; set; }

        public DbSet<EventType> EventType { get; set; }

        public DbSet<CustomNotification> CustomNotification { get; set; }

        public DbSet<Subscription> Subscription { get; set; }

        public DbSet<TemblarUser> TemblarUser { get; set; }

        public DbSet<Source> Source { get; set; }

        public DbSet<SourceSubscription> SourceSubscription { get; set; }

        public DbSet<ClientDevice> ClientDevice { get; set; }

        public TemblarDbContext(DbContextOptions opt) : base(opt)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subscription>().HasKey(subscription => new { subscription.TemblarUserId, subscription.EventTypeId });
            modelBuilder.Entity<SourceSubscription>().HasKey(sourceSubscription => new { sourceSubscription.TemblarUserId, sourceSubscription.SourceId });
            modelBuilder.Entity<ClientDevice>().Property(cd => cd.CreatedAt).HasDefaultValue(new DateTimeOffset(DateTime.Now));
        }


    }
}
