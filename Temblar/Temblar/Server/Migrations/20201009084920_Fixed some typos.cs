﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Temblar.Server.Migrations
{
    public partial class Fixedsometypos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_EventType_EventTypeId",
                table: "Event");

            migrationBuilder.DropForeignKey(
                name: "FK_Subscription_TemblarUser_TemblarUserId",
                table: "Subscription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription");

            migrationBuilder.DropIndex(
                name: "IX_Subscription_TemblarUserId",
                table: "Subscription");

            migrationBuilder.DropColumn(
                name: "TemlbarUserId",
                table: "Subscription");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "Event");

            migrationBuilder.AlterColumn<Guid>(
                name: "TemblarUserId",
                table: "Subscription",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "EventTypeId",
                table: "Event",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription",
                columns: new[] { "TemblarUserId", "EventTypeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Event_EventType_EventTypeId",
                table: "Event",
                column: "EventTypeId",
                principalTable: "EventType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Subscription_TemblarUser_TemblarUserId",
                table: "Subscription",
                column: "TemblarUserId",
                principalTable: "TemblarUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_EventType_EventTypeId",
                table: "Event");

            migrationBuilder.DropForeignKey(
                name: "FK_Subscription_TemblarUser_TemblarUserId",
                table: "Subscription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription");

            migrationBuilder.AlterColumn<Guid>(
                name: "TemblarUserId",
                table: "Subscription",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "TemlbarUserId",
                table: "Subscription",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "EventTypeId",
                table: "Event",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "EventId",
                table: "Event",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subscription",
                table: "Subscription",
                columns: new[] { "TemlbarUserId", "EventTypeId" });

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_TemblarUserId",
                table: "Subscription",
                column: "TemblarUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Event_EventType_EventTypeId",
                table: "Event",
                column: "EventTypeId",
                principalTable: "EventType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subscription_TemblarUser_TemblarUserId",
                table: "Subscription",
                column: "TemblarUserId",
                principalTable: "TemblarUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
