﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.DTOs
{
    public class UnseenNotifications
    {
        public int Unseen { get; set; }
    }
}
