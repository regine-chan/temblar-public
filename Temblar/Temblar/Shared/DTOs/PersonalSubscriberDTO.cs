﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.DTOs
{
    public class PersonalSubscriberDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HashSet<string> SubscribedSources { get; set; }
    }
}
