﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.DTOs
{
    public class PushNotificationDto
    {
        public string Email { get; set; }

        public Notification Notification { get; set; }
    }
}
