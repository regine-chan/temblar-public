﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.DTOs
{
    public class Notification
    {
        // Id for notifikasjon som tilhører spesifikk kunde
        public Guid Id { get; set; }

        // Navn for hendelse f.eks. Ny sak registrert
        public string DisplayName { get; set; }

        // Navn på systemet som sender hendelsen f.eks recall
        public string Source { get; set; }

        // Ekstra felt som beskriver hendelsen, f.eks. sakId=12
        public dynamic Payload { get; set; }

        // Navn/epost på den som aktiverte hendelsen
        public string TriggeredBy { get; set; }

        // Tidspunkt når hendelsen skjedde
        public DateTimeOffset TriggeredAt { get; set; }

        // Tidspunkt når bruker leser notifikasjon
        public DateTimeOffset? SeenAt { get; set; }

        // Tidspunkt når varsel er lest/dismissed?
        public DateTimeOffset? ReadAt { get; set; }

        // Beskrivelse av eventen
        public string DisplayMessage { get; set; }

        // Url til info om hendelsen
        public string UserInterfaceUrl { get; set; }

    }
}
