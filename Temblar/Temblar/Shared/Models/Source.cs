﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Temblar.Shared.Models
{
    public class Source
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string SourceName { get; set; }

        [JsonIgnore]
        public IList<SourceSubscription> SourceSubscriptions { get; set; }

        public IList<Event> Events { get; set; }
    }
}
